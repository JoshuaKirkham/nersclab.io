## Description and Overview
Climate Data Operators (CDO) is a large toolset for working on climate data. [NetCDF](https://www.nersc.gov/users/data-analytics/data-management/i-o-libraries/netcdf-2/netcdf/) version 3 and 4, GRIB including SZIP compression, EXTRA, SERVICE and IEG formats are supported. CDO can also be used to analyse any kind gridded data not related to climate science.
CDO has very small memory requirements and can process files larger than the physical memory.

## How to Use CDO

```
module load cdo
cdo [options] Operators ...
```

## Documentation
[CDO Wiki](https://code.zmaw.de/projects/cdo/wiki/Cdo#Documentation)

## Availability

1.6.5.2, 1.7.0, 1.9.3(default)
